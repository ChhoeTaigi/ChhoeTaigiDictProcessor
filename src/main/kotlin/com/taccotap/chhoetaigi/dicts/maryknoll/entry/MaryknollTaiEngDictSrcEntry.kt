package com.taccotap.chhoetaigi.dicts.taibuntiongbun.entry

import com.taccotap.chhoetaigi.entry.DictEntry

open class MaryknollTaiEngDictSrcEntry : DictEntry() {
    lateinit var id: String
    lateinit var poj: String
    lateinit var hoabun: String
    lateinit var englishDescriptions: String
    lateinit var pageNumber: String
}