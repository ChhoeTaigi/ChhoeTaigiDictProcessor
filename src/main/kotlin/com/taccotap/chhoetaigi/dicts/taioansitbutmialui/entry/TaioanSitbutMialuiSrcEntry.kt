package com.taccotap.chhoetaigi.dicts.taioansitbutmialui.entry

import com.taccotap.chhoetaigi.entry.DictEntry

open class TaioanSitbutMialuiSrcEntry : DictEntry() {
    lateinit var id: String
    lateinit var kiplmj: String
    lateinit var hanjiTaibun: String
    lateinit var pageNumber: String
}