package com.taccotap.chhoetaigi.dicts.taioansitbutmialui.entry

import com.taccotap.chhoetaigi.entry.DictEntry

open class TaioanSitbutMialuiOutEntry : DictEntry() {
    lateinit var id: String

    lateinit var pojUnicode: String
    lateinit var pojInput: String

    lateinit var kiplmjUnicode: String
    lateinit var kiplmjInput: String

    lateinit var hanjiTaibun: String

    lateinit var pageNumber: String
}