package com.taccotap.chhoetaigi.dicts.itaigi.entry

import com.taccotap.chhoetaigi.entry.DictEntry

open class ITaigiDictSrcEntry : DictEntry() {
    lateinit var kiplmj: String
    lateinit var hanloTaibunKiplmj: String
    lateinit var hoabun: String
    lateinit var from: String
}