package com.taccotap.chhoetaigi.dicts.taioanpehoegiku.entry

import com.taccotap.chhoetaigi.entry.DictEntry

open class TaioanPehoeKichhooGikuSrcEntry : DictEntry() {
    lateinit var id: String
    lateinit var kiplmj: String
    lateinit var kiplmjDialect: String
    lateinit var hoabun: String
    lateinit var pageNumber: String
}