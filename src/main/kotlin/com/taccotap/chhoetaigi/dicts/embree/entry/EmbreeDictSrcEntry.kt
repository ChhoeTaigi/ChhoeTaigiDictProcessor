package com.taccotap.chhoetaigi.dicts.embree.entry

import com.taccotap.chhoetaigi.entry.DictEntry

open class EmbreeDictSrcEntry : DictEntry() {
    lateinit var poj: String
    lateinit var hoabun: String
    lateinit var abbreviations: String
    lateinit var nounClassifiers: String
    lateinit var reduplication: String
    lateinit var englishDescriptions: String
    lateinit var pageNumber: String
}